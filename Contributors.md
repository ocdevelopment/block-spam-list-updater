Contributors
------------
These people have contributed to Blocklist-Sync and implementation:

  * [MSIDoc](https://bitbucket.org/MSIDoc/) - Developer
  * [DarkDelta](https://bitbucket.org/darkdelta/) - Tester