#!/bin/bash 
################################################################################################################
#                                                                                                              #
# = = = = = = = = = = = = = = = = = = = = = = Installer & Importer = = = = = = = = = = = = = = = = = = = = = = #
# = = = = = = = = = = = = = = = = = = = = from Blocklist.de & Spamhaus = = = = = = = = = = = = = = = = = = = = #
#                                                                                                              #
# Author:   MSIDoc, http://omegacrew.de                                                                        #
# Date:     2016-02-26                                                                                         #
# Updated:  2020-01-13 21-35                                                                                   #
# Version:  @VERSION                                                                                           #
# License:  Creative Commons Namensnennung - Nicht kommerziell - Keine Bearbeitungen 4.0 International Lizenz  #
#                 [ http://creativecommons.org/licenses/by-nc-nd/4.0/deed.de ]                                 #
#                                                                                                              #
################################################################################################################
VERSION="0.40 rev.140"
COPYRIGHT="Copyright (c) 2008 - $(date '+%Y') by OmegaCrew Development (March 28th 2016)."
started=`date +%s`
### BEGIN CONFIG BODY ##########################################################################################
#
# SERVICES        Wahl des abzurufenden Services. Es koennen einer oder mehrere eingetragen werden.
#                 OPTIONS:      ssh, apache, imap, pop3, mail, ftp, all
#                 DEFAULT:      all
#
# TIME            Argument, mit dem der Service abgerufen wird. Der Zeitwert ist in Sekunden anzugeben.
#                 Auf hinsicht der aufkommenden Serverlast, ist hier der Optimal-Wert anzustreben.
#                 MIN:  1800 === >> Optimal:   3600 =============== >>>>>>>>>>> MAX:  86400
#                 DEFAULT: 3600  => 1 Stunde
#
# CHAINNAME       Chain-Name welcher in der Iptable vorhanden sein muss und ist auch der Name der Jail Regel fuer F2B.
#                 DEFAULT:      blocklist-de
#
# PRINT_REPORT    [optional] Log-Report dieses Script wird in einer Logdatei gespeichert
#                 OPTION:       0 => FALSE || 1 => TRUE
#                 DEFAULT:      0
#
#
SERVICES=( 'all' )
TIME=3600
CHAINNAME="blocklist-de"
PRINT_REPORT=1

### BLOCKLIST.DE JAIL CONFIG ###################################################################################
#
# enabled         Aktiviert die Jail Regel in F2B
#                 DEFAULT:      true
#
# port            definiert den Port fuer die Regel welcher ueberwacht und im Falle eines Events gesperrt werden soll
#                 NOTE:    Man kann mehrere Services oder Ports getrennt durch ein (,) ohne Leerzeichen angeben.
#                          Oder `all` wenn man alle Ports nominieren moechte.
#                 DEFAULT:      all
#
# filter          definiert den Filter nach dem die Regel abgearbeitet werden soll
#                 DEFAULT:      sshd
#
# logpath         legt den Pfad zur Logdatei sowie den Namen dieser fest
#                 DEFAULT:      /var/log/blocklist-de.log
#
# maxretry        legt die maximale Anzahl der eintraege in der Jail-Logdatei fest
#                 OPTIMAL:      1
#
# bantime         Legt die Zeitspanne fuer die zur Sperrende IP-Adresse fest.
#                 Der Wer ist in Sekunden anzugeben, hierbei entspricht 86400 => 1 Tag, 604800 => 1 Woche, 2419200 => 1 Monat oder 7257600 => 3 Monate
#                 DEFAULT:      604800
#
enabled="true"
port="ssh"
filter="sshd"
logpath="/var/log/$CHAINNAME.log"
maxretry=1
bantime=2419200


########## Do not edit anything below this line ##########

### BEGIN SCRIPT BODY ##########################################################################################

  # ich bin der Script Body

### BEGIN FUNKTION BODY ########################################################################################
### BEGIN FUNKTION CHECKLIST #####################################################
function checklist(){

## Pruefen der angewandten Tools
# desc: Prueft ob alle benoetigten Tools die fuer dieses Script benoetigt werden, auch installiert sind.
#       Andernfalls wird auf die fehlenden Tools hingewiesen und Option der Nachinstallation angeboten.
#
# dependencies
DEPENDS=( 'wget' 'mktemp' 'sort' 'uniq' )
for dep in ${DEPENDS[@]}
do
  if ! type -p ${dep} >/dev/null
  then
    echo -e "Die Anwendung ${dep} konnte nicht gefunden werden. \nBitte installieren Sie das fehlende Paket ${dep} nach & fuehren Sie '$0 --checklist' danach erneut aus." >&2
    read -p "Moechtest Sie das fehlende Paket ${dep}, jetzt installieren?(J/n) " choice
    [ "${choice}" != "J" ] && exit 0
      apt update && apt install ${dep}
  fi
done

## Pruefen der Fail2Ban Installation
# desc: Prueft ob Fail2Ban vorhanden ist. Sonst wird automatische Nachinstallation angeboten oder 
#       auf manuelle Installation hingewiesen.
#
if [ -d /etc/fail2ban ] ; then
  echo "Toolcheck-Fail2Ban wurde positiv bestaetigt"
else
  echo -e "Die Anwendung "Fail2Ban" konnte nicht gefunden werden. \nBitte installieren Sie das fehlende Paket Fail2Ban nach & fuehren Sie '$0 --checklist' danach erneut aus." >&2
  read -p "Moechtest Sie das fehlende Paket "Fail2Ban", jetzt installieren?(J/n) " choice
  [ "${choice}" != "J" ] && exit 0
    apt update && apt install fail2ban
  exit 1
fi

## Pruefen der Fail2Ban Konfigurationsdateien.
# desc: Prueft ob die Konfigurationsdatei *.conf und/oder *.local vorhanden ist
#
if [ -f /etc/fail2ban/jail.conf ] ; then
  echo -e "\033[0m Jail-Konfigurationsdatei vorhanden.\033[0;1m (/etc/fail2ban/jail.conf)"
  if [ -f /etc/fail2ban/jail.local ] ; then
    echo -e "\033[0m Benutzerdefinierte Jail-Konfigurationsdatei vorhanden.\033[0;1m (/etc/fail2ban/jail.local)"
  else
    echo -e "\033[0;41m Benutzerdefinierte Jail-Konfigurationsdatei fehlt.\033[0;1m (/etc/fail2ban/jail.local)"
    cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local && sleep 1
    echo -e "\033[0m Benutzerdefinierte Jail-Konfigurationsdatei wurde neu erzeugt"
  fi	 
else
  echo -e "\033[1;41m ERROR \033[0;1m Jail-Konfigurationsdatei fehlt.\033[0;1m (\"/etc/fail2ban/jail.conf\")\033[0m"
  exit 1
fi

## Ueberpruefe ob Chain in Iptable registriert ist
#
if [ `iptables --list INPUT | grep fail2ban-$CHAINNAME | wc -l` -eq 1 ]
then
  echo -e "\033[0m Regel \033[0;1m[$CHAINNAME]\033[0m vorhanden und aktiviert. "
else
	# User darauf hinweisen das f2b-filter nicht geladen oder nicht installiert ist.
  echo "Bitte ueberpruefen Sie die '/etc/fail2ban/jail.local' ob nachstehende Regel : [$blocklist] eingetragen & aktiviert ist."
  echo -e "\n[$blocklist] \nenabled   = $enabled \nport      = $port \nfilter    = $filter \nlogpath   = $logpath \nmaxretry  = $maxretry \nbantime   = $bantime \naction    = %(action_)s"
  echo "Oder fuehren Sie '$0 --entry-jail' aus. Hierdurch wird obenstehende Regel eingetragen & aktiviert"
  echo "Anschliessend starten Sie '$0 --checklist' erneut"
  exit 1
fi

}
### END FUNKTION CHECKLIST #######################################################

## benoetigte Variablen
#
PrintLogPath="/root/blocklist_logs"
Print_Log="blocklist_$(date +%Y%m%d%H%M%S -d "@$started").log"
blocklist=$CHAINNAME
countAfterSortAndUnique=0
countInserted=0
ipList=`mktemp`             # Alternativ /tmp/blocklist-ips-unfiltered.txt
clearIpList=`mktemp`      # Alternativ /tmp/blocklist-ips-filtered.txt

### BEGIN FUNKTION RUN ###########################################################
function run() {

## Laden aller angegebenen Services
for service in ${SERVICES[@]}
do
  url="http://api.blocklist.de/getlast.php?time=$TIME&service=$service"
  if [[ $service ==  "ssh" ]] || [[ $service ==  "apache" ]] || 
     [[ $service ==  "imap" ]] || [[ $service ==  "pop3" ]] || 
     [[ $service ==  "mail" ]] || [[ $service ==  "ftp" ]] || 
     [[ $service ==  "all" ]]
  then
    wget -O -  $url >> $ipList
  else
    echo "Konnte Service \"$service\" nicht vom Blocklist.de Server abrufen!"
  fi
done

## Sort and filter by Uniq
cat $ipList | sort | uniq > $clearIpList

countAfterSortAndUnique=`cat $clearIpList | wc -l`


## Ueberpruefen ob geladene IP-Adressen von 'Blocklist.DE' bereits in der `iptables` geladen sind.
while read currentIP
do
  # 0 = Regel fuer die IP-Adresse ist nicht vorhanden und muss fuer `iptables` aufbereitet werden.
  # 1 = Regel fuer die IP-Adresse ist vorhanden und kann aus cache geloescht werden.
  if [ `iptables --list -v -n | grep $currentIP | wc -l` -eq 0 ]
  then
    fail2ban-client set $blocklist banip $currentIP	

    # Increment the counter
    countInserted=$((countInserted + 1))
  fi

done < $clearIpList

report
cleanup
}
### END FUNKTION RUN #############################################################

### BEGIN FUNKTION REPORT ########################################################
function report() {
## Print report
if [ $PRINT_REPORT -eq 1 ]
then
  if ! type -d $PrintLogPath ; then
    mkdir $PrintLogPath
  fi
  echo "--- Blockliste.de :: Update-Report" >> $Print_Log
  echo "" >> $Print_Log
  echo "Script Version:                 $VERSION" >> $Print_Log
  echo "Reported Service:               $SERVICES" >> $Print_Log
  echo "Started:                        $(date -d "@$started")" >> $Print_Log
  echo "Finished:                       `date`" >> $Print_Log
  echo "" >> $Print_Log
  echo "--> Reported IPs:               $countAfterSortAndUnique" >> $Print_Log
  echo "--> Added IPs:                  $countInserted" >> $Print_Log
  echo "" >> $Print_Log
  echo "$COPYRIGHT" >> $Print_Log
fi

}
### END FUNKTION REPORT ##########################################################

### BEGIN FUNKTION CHECK_ROOT ####################################################
## pruefen auf Root-Access
function checkroot() { 
  if [ ${UID} -ne 0 ] ; then
    echo -e "Root privileges needed. Exit.\n" 
    exit 0
  fi
}
### END FUNKTION CHECK_ROOT ######################################################

### BEGIN FUNKTION ENTRY_JAIL ####################################################
## Regel zur jail.local hinzufuegen
function entry_jail() {
CONF="/etc/fail2ban/jail.local"
check_root
echo "erstelle $logpath" && touch $logpath
echo "erstelle neue Regel [$CHAINNAME] in Benutzerdefinierter Jail-Konfigurationsdatei"
cat << EOF >> $CONF

[$CHAINNAME]
enabled         = $enabled
port            = $port
filter          = $filter
logpath         = $logpath
maxretry        = $maxretry
bantime         = $bantime
action          = %(action_)s

EOF

service fail2ban restart && sleep 2
echo -e "\nAktivierte Jail Regeln" && fail2ban-client status
exit 0
}
### BEGIN FUNKTION ENTRY_JAIL ####################################################

### END FUNKTION HELP ############################################################
function help(){
	echo $COPYRIGHT
	echo $VERSION
	echo " "" Aufruf: $0 [OPTION]... "
	echo ""
	echo " "" --entry-jail                     Traegt die fehlende Fail2Ban Regel nach."
	echo " "" --first-run                      Nur zu nutzen wenn dieses Script zum ersten mal gestartet wird."
	echo " "" --start-blocklist                Initiiert den download der IP-Adress Liste."
	echo " "" --help                           diese Hilfe anzeigen und beenden."
	echo " "" --version                        Display the Copyright, the Version and Revision-No."
	exit 1
	
	# Hilfe muss nocheinmal ueberarbeitet werden
	# oder es sollte gar eine richtige MANPAGE geschrieben werden
	# ...
	# etc.
}
### END FUNKTION HELP ############################################################

### BEGIN FUNKTION CLEANUP #######################################################
## Cleanup && CLOSE
function cleanup() { 
  rm -f $ipList
  rm -f $clearIpList
  
  exit 0
}
### END FUNKTION CLEANUP #########################################################

## externer zugriff auf Funktionen oder Erklaerung
case "$1" in
  --checklist)
    checklist
  ;;
  --entry-rule)
    entry_jail
  ;;
  --first-run)
    checkroot
    checklist
    run
  ;;
  --start-blocklist)
    run
  ;;
  -h | --help | *)
    checkroot
    help
  ;;
esac


# END_OF_FILE