### Overview
Ich stelle hier ein Script vor, dass die als Spam und/oder attackierend gemeldete IP-Adressen von [http://blocklist.de](http://blocklist.de "blocklist.de -- Fail2Ban-Reporting Service (Attacks on Postfix, SSH, Apache-Attacks, Spambots, irc-Bots, Reg-Bots, DDos and more) from report SSH-, Mail-, FTP-, Apache- and other Attacks") herunterlädt und mittels Fail2Ban-Client in die Firewall lädt.

##### Features

* eigene Abhängigkeiten überprüfen & gegebenenfalls nach Installieren
* Einstellungsmöglichkeit für zu ladende Liste(n)
* Doublettenerkennung bei Verwendung mehrerer Listen
* Eigene Chain in ```iptables``` – Andere Chains bleiben unberührt
* IP-Adressen abgleich mit den aus ```iptables``` hinterlegten und den Automatisch neugeladenen
* Einfaches ausführen per Cron möglich

##### Abhängigkeiten
* Root-User Privilegien
* `mktemp`, `wget`, `git`, `sort`, `uniq` & `fail2ban-`
* Fail2Ban **-stable** aus der aktuellen Repository
* **Nightly oder Develop** Versionen sind auf eigene Gefahr zu nutzen.
- Da diese noch fehlerbehaftet sein können und wir für die Kompatibilität & Funktionalität keine Gewährleistungen übernehmen

### Installation

Checke einfach die Repository aus 
```sh
$ cd ~ 
$ mkdir repos
$ cd repos
$ git clone git@bitbucket.org:MSIDoc/block-spam-list-updater.git
```
kopiere die Datei ```blocklist-update.sh``` aus der Repository in das ```/sbin``` Verzeichnis und mache sie ausführbar.
```sh
$ cd block-spam-list-updater/ && ls -la
$ cp blocklist-update.sh /sbin/ && cd /sbin/
$ chmod 700 ./blocklist-update.sh
```
Danach folgenden Eintrag die root-Crontab unter /etc/crontab eintragen:

```sh
0 */1 * * * root /sbin/blocklist-update.sh
```

Somit wird das Script jede Stunde ausgeführt.

In der Datei selbst kann man die einzelnen Listen hinzufügen, die man haben will. Theoretisch werden auch Listen von anderen Anbietern akzeptiert, wenn pro Zeile eine IP-Adresse steht. Ich spreche trotzdem meine Empfehlung für die Verwendung von blocklist.de aus!

[Hier findet man alle angebotenen Listen.](http://www.blocklist.de/de/export.html "Export aller geblockten IPs")


### Source Code
Da es immer mal wieder zu Formatierungsproblemen beim Code in Verbindung mit WordPress kommt, sollte diese nur aus dieser Repository geladen und installiert werden.

**Hinweis:**
> Das Script benötigt root-Rechte, da es mit iptables-Befehlen arbeitet.

> Beim ersten mal ausführen wird das Script je nach Anzahl IP-Adressen ziemlich lange benötigen. Beim zweiten mal werden nur noch neue IPs eingetragen und veraltete entfernt. Dadurch erhöht sich die Scriptgeschwindigkeit.

> Alle Intervalle unter 30 Minuten sind entziehen sich der Logik, da die Listen selbst nur alle 30 Minuten (+/- 5 Minuten) neu erstellt werden.

### Limitations 
 Dieses Script wurde auf folgenden Server Systemen getestet:

* Fail2Ban 0.8.6-3 / 0.8.11-1 & 0.8.13-1
* Debian 8
* Ubuntu 12.04 LTS/14.04 LTS

### Development

##### Bug Report

Wenn Sie einen Fehler gefunden, wenn Sie Fragen zur Dokumentation oder Fragen zu diesem Script haben - so erstelle bitte ein neues Ticket zu der Frage oder Fehler.

##### Pull Request

Wenn Sie in der Lage sind, den Fehler zu korrigieren oder sinnvolle Erweiterungen einbinden möchten - dann sende bitte eine Pull-Anforderung an uns.

##### Contributors

Die Liste der Mitwirkenden kann hier gefunden werden: [Contributors](https://bitbucket.org/MSIDoc/block-spam-list-updater/src/d11e28d4ac5ebeab1ab21fa40ffeb85516c9b931/Contributors.md?at=master&fileviewer=file-view-default)